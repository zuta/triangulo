#!/usr/bin/env pythoh3


import sys

number = 4

def line(number: int):
    repetir = str(number) * number
    return repetir
def triangle(number: int):
    if number >= 9:
        raise ValueError('El número es mayor de o igual a 9')
    triangulo = ""
    for i in range (1, number + 1):
        triangulo = str(triangulo) + line(i) + '\n'

    return triangulo

def main():

    number: int = sys.argv[1]
    text = triangle(int(number))
    print(text)

if __name__ == '__main__':
    main()